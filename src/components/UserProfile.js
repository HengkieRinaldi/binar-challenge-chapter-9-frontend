import { useState, useEffect } from "react";
import axios from "axios";
import { Table, Card } from "react-bootstrap";
import jwt from "jwt-decode";
import { useCookies } from "react-cookie";

const UserProfile = () => {
  const [cookies, setCookies] = useCookies();
  const [first_name, setFirst_name] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [birth, setBirth] = useState("");
  const [gender, setGender] = useState("");
  const [address, setAddress] = useState("");
  const [userHistory, setUserHistory] = useState([]);
  let w = 0;
  let d = 0;
  let l = 0;

  if (userHistory) {
    for (let i = 0; i < userHistory.length; i++) {
      w += userHistory[i].win;
      d += userHistory[i].draw;
      l += userHistory[i].lose;
    }
  }

  useEffect(() => {
    getUserById();
    getHistoryByUserId();
  }, []);

  const getUserById = async () => {
    const token = cookies.token;
    const decodeToken = jwt(token);
    const id = decodeToken.id;

    const response = await axios.get(`/api/user/${id}`, {
      withCredentials: false,
    });
    console.log(response.data);
    setFirst_name(response.data.first_name);
    setUsername(response.data.username);
    setEmail(response.data.email);
    setPhone(response.data.phone);
    setBirth(response.data.birth);
    setGender(response.data.gender);
    setAddress(response.data.address);
  };

  const getHistoryByUserId = async () => {
    const token = cookies.token;
    const decodeToken = jwt(token);
    const user_id = decodeToken.id;
    const response = await axios.get(`/api/history/${user_id}`, {
      withCredentials: false,
    });
    console.log(response.data.result);
    setUserHistory(response.data.result);
    for (let i = 0; i < response.data.result.length; i++) {
      console.log("test", [i]);
    }
  };
  return (
    <div>
      <div>
        <Card style={{ width: "18rem" }}>
          <Card.Body>
            <Card.Title>Name</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">
              {first_name}
            </Card.Subtitle>
          </Card.Body>
          <Card.Body>
            <Card.Title>Username</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">
              {username}
            </Card.Subtitle>
          </Card.Body>
          <Card.Body>
            <Card.Title>Email</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{email}</Card.Subtitle>
          </Card.Body>
          <Card.Body>
            <Card.Title>Phone</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{phone}</Card.Subtitle>
          </Card.Body>
          <Card.Body>
            <Card.Title>Date Of Birth</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{birth}</Card.Subtitle>
          </Card.Body>
          <Card.Body>
            <Card.Title>Gender</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{gender}</Card.Subtitle>
          </Card.Body>
          <Card.Body>
            <Card.Title>Address</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{address}</Card.Subtitle>
          </Card.Body>
        </Card>
      </div>
      <div>
        <h1>Game History</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th className="justify-content-center text-center">User</th>
              <th className="justify-content-center text-center">Win</th>
              <th className="justify-content-center text-center">Draw</th>
              <th className="justify-content-center text-center">Lose</th>
              <th className="justify-content-center text-center">Scheme</th>
              <th className="justify-content-center text-center">Openent</th>
              <th className="justify-content-center text-center">Time Stamp</th>
            </tr>
          </thead>
          <tbody>
            {userHistory.map((a, b) => (
              <tr key={b}>
                <td>{a.username}</td>
                <td>{a.win}</td>
                <td>{a.draw}</td>
                <td>{a.lose}</td>
                <td>{a.scheme}</td>
                <td>{a.oponent}</td>
                <td>{a.timestamp}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <div>
        <h1>Game Summary</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th className="justify-content-center text-center">User</th>
              <th className="justify-content-center text-center">Win</th>
              <th className="justify-content-center text-center">Draw</th>
              <th className="justify-content-center text-center">Lose</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{username}</td>
              <td>{w}</td>
              <td>{d}</td>
              <td>{l}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default UserProfile;
