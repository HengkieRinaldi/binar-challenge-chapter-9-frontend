import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

const EditUser = () => {
  const [first_name, setFirst_name] = useState("");
  const [last_name, setLast_name] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPasword] = useState("");
  const [phone, setPhone] = useState("");
  const [birth, setBirth] = useState("");
  const [address, setAddress] = useState("");
  const [gender, setGender] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    getUserById();
  }, []);

  const getUserById = async () => {
    const response = await axios.get(`/api/user/${id}`, {
      withCredentials: false,
    });
    console.log(response.data);
    setFirst_name(response.data.first_name);
    setLast_name(response.data.last_name);
    setUsername(response.data.username);
    setEmail(response.data.email);
    setPasword(response.data.password);
    setPhone(response.data.phone);
    setBirth(response.data.birth);
    setGender(response.data.gender);
    setAddress(response.data.address);
  };

  const updateUser = async (e) => {
    e.preventDefault();
    try {
      await axios.put(
        `/api/user/${id}`,
        {
          first_name,
          last_name,
          username,
          email,
          password,
          birth,
          address,
          gender,
          phone,
        },
        {
          withCredentials: false,
        }
      );
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="columns mt-5">
      <div className="column is-half">
        <form onSubmit={updateUser}>
          <div className="field">
            <label className="label">Username</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                placeholder="Name"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">first name</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={first_name}
                onChange={(e) => setFirst_name(e.target.value)}
                placeholder="Name"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">last name</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={last_name}
                onChange={(e) => setLast_name(e.target.value)}
                placeholder="Name"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="Email"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">password</label>
            <div className="control">
              <div className="control">
                <input
                  type="text"
                  className="input"
                  value={password}
                  onChange={(e) => setPasword(e.target.value)}
                  placeholder="password"
                />
              </div>
            </div>
          </div>
          <div className="field">
            <label className="label">phone</label>
            <div className="control">
              <div className="control">
                <input
                  type="text"
                  className="input"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                  placeholder="password"
                />
              </div>
            </div>
          </div>
          <div className="field">
            <label className="label">birth</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={birth}
                onChange={(e) => setBirth(e.target.value)}
                placeholder="Name"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">gender</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={first_name}
                onChange={(e) => setGender(e.target.value)}
                placeholder="Name"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">address</label>
            <div className="control">
              <input
                type="text"
                className="input"
                value={first_name}
                onChange={(e) => setAddress(e.target.value)}
                placeholder="Name"
              />
            </div>
          </div>
          <div className="field">
            <div className="control">
              <button type="submit" className="button is-success">
                Update
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditUser;
