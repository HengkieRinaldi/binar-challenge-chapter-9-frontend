import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import { Button, Table } from "react-bootstrap";

const Admin = () => {
  const [users, setUsers] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    getUser();
  }, []);
  const getUser = async () => {
    const response = await axios.get(`/api/user`, {
      headers: { "Content-Type": "application/json" },
      withCredentials: false,
    });
    console.log(response.data);
    setUsers(response.data);
  };
  const deleteUser = async (id) => {
    try {
      await axios.delete(`/api/user/${id}`, {
        withCredentials: false,
      });
      getUser();
    } catch (err) {
      console.log(err.message);
    }
  };

  return (
    <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th className="justify-content-center text-center">No</th>
            <th className="justify-content-center text-center">User Name</th>
            <th className="justify-content-center text-center">Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.map((a, b) => (
            <tr key={b}>
              <td>{b + 1}</td>
              <td>{a.username}</td>
              <td>{a.email}</td>
              <td>
                <Link to={`/user-edit/${a._id}`}>
                  <Button variant="primary">update</Button>
                </Link>
                <Button onClick={() => deleteUser(a._id)} variant="danger">
                  {" "}
                  delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Admin;
