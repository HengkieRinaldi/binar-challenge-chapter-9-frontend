import React, { useState, useEffect } from "react";
import "../assets/styles/Homepage.css";
import NavigationBar from "../components/Navbar/NavigationBar";
import Cards from "../components/Card/Cards";
import CarouselBox from "../components/Card/CarouselBox";
import Footer from "../components/Footer/Footer";
import jwt from "jwt-decode";
import { useCookies } from "react-cookie";

const Homepage = () => {
  const [cookies] = useCookies();
  const [gameRoute, setGameRoute] = useState("/login");
  const [loginState, setLoginState] = useState(false);

  useEffect(() => {
    getUserById();
  }, []);

  const getUserById = async () => {
    const token = cookies.token;
    const decodeToken = jwt(token);
    const id = decodeToken.id;
    id ? setGameRoute("/game") : setGameRoute("/login");
    id ? setLoginState(true) : setLoginState(false);
  };

  function playBtn() {
    loginState ? console.log("masuk") : alert("you Haven't login yet!!");
  }

  return (
    <div id="homepage">
      <div className="home-bg">
        <NavigationBar />
        <div className="row align-items-center" id="main-bg">
          <div className="col-12 text-center">
            <h1>PLAY TRADITIONAL GAME</h1>
            <p className="my-4">Experience new traditional game</p>
            <button
              type="button"
              onClick={() => playBtn()}
              className="btn btn-warning btn-sm mt-2 px-5"
            >
              <small>
                <a href={gameRoute}>PLAY NOW</a>
              </small>
            </button>
          </div>
          <div className="thestory text-center">
            <p>THE STORY</p>
            <img
              src={require("../assets/img/scroll-down.svg").default}
              alt=""
            />
          </div>
        </div>
      </div>

      <div className="container" id="section2">
        <div className="row">
          <div className="col-md-3 offset-1">
            <p>What's so special?</p>
            <h1>THE GAMES</h1>
          </div>
          <div className="col-md-7">
            <CarouselBox />
          </div>
        </div>
      </div>

      <div className="container" id="section3">
        <div className="row">
          <div className="col-sm-4 offset-7">
            <p className="top-p">What's so special?</p>
            <h1>FEATURES</h1>
            <ul className="list-features">
              <li className="detail-list">
                <h5>TRADITIONAL GAME</h5>
                <p>
                  If you miss your childhood, we provide many traditional games
                  here
                </p>
              </li>
              <li>GAME SUIT</li>
              <li>TBD</li>
            </ul>
          </div>
        </div>
      </div>

      <div className="container" id="section4">
        <div className="row">
          <p className="text-center">Can My Computer Run This Game?</p>
          <div className="col-sm-5 offset-1">
            <h1>SYSTEM REQUIREMENT</h1>
            <table className="table table-bordered">
              <tr>
                <td>
                  <p className="text-orange">OS:</p>
                  <p>
                    Windows 7 64-bit only
                    <br />
                    (No OSX support at this time)
                  </p>
                </td>
                <td>
                  <p className="text-orange">PROCESSOR:</p>
                  <p>Intel Core 2 Duo @ 2.4 GHZ or AMD Athlon X2 @ 2.8 GHZ </p>
                </td>
              </tr>
              <tr>
                <td>
                  <p className="text-orange">MEMORY:</p>
                  <p> 4 GB RAM </p>
                </td>
                <td>
                  <p className="text-orange">STORAGE:</p>
                  <p> 8 GB available space </p>
                </td>
              </tr>
              <tr>
                <td colSpan="2">
                  <p className="text-orange">GRAPHICS:</p>
                  <p>
                    {" "}
                    NVIDIA GeForce GTX 660 2GB or
                    <br />
                    AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)
                  </p>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div className="container" id="section5">
        <div className="row">
          <div className="col-sm-4 offset-1">
            <h1>TOP SCORES</h1>
            <p>This top score from various games provided on this platform</p>
            <button type="button" className="btn btn-md px-5 btn-warning">
              see more
            </button>
          </div>
          <div className="col-sm-5 offset-1">
            <Cards />
          </div>
        </div>
      </div>

      <div className="container" id="section6">
        <div className="row">
          <div className="col-md-3 offset-2">
            <img src={require("../assets/img/skull.png")} alt="" />
          </div>
          <div className="col-md-6 offset-1">
            <p className="top-text">
              Want to stay in
              <br />
              touch?
            </p>
            <h1>NEWSLETTER SUBSCRIBE</h1>
            <p className="desc">
              In order to start receiving our news, all you have to do is enter
              your email address. Everything else will be taken care of by us.
              We will send you emails containing information about game. We
              don't spam.
            </p>
            <div className="row justify-content-start text-white fw-bold">
              <form>
                <div className="row g-3 align-items-center">
                  <div className="col-auto">
                    <input
                      type="email"
                      placeholder="Your email address"
                      className="form-control px-5 py-2"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                    />
                  </div>
                  <div className="col-auto">
                    <button
                      type="submit"
                      className="btn btn-warning px-4 py-2 btn-sm"
                    >
                      Subscribe now
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Homepage;
