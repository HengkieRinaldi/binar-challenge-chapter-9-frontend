import { useRef, useState, useEffect } from "react";
import {
  faCheck,
  faTimes,
  faInfoCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import "../assets/styles/Signup.css";

const USER_REGEX = /^[A-z][A-z0-9-_]{2,23}$/;
const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,24}$/;
const EMAIL_REGEX =
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PHONE_REGEX = /^\d{11,13}$/;
const ADDRESS_REGEX = /^[a-zA-Z0-9\s,.'-]{3,}$/;
const BIRTH_REGEX =
  /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
const REGISTER_URL = "/api/user";

const Register = () => {
  const userRef = useRef();
  const errRef = useRef();

  const [first_name, setFn] = useState("");
  const [validFn, setValidFn] = useState(false);
  const [fnFocus, setFnFocus] = useState(false);

  const [last_name, setLn] = useState("");
  const [validLn, setValidLn] = useState(false);
  const [lnFocus, setLnFocus] = useState(false);

  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);

  const [address, setAddress] = useState("");
  const [validAddress, setValidAddress] = useState(false);
  const [addressFocus, setAddressFocus] = useState(false);

  const [phone, setPhone] = useState("");
  const [validPhone, setValidPhone] = useState(false);
  const [phoneFocus, setPhoneFocus] = useState(false);

  const [birth, setBirth] = useState("");
  const [validBirth, setValidBirth] = useState(false);
  const [birthFocus, setBirthFocus] = useState(false);

  const [username, setUser] = useState("");
  const [validName, setValidName] = useState(false);
  const [userFocus, setUserFocus] = useState(false);

  const [password, setPwd] = useState("");
  const [validPwd, setValidPwd] = useState(false);
  const [pwdFocus, setPwdFocus] = useState(false);

  const [matchPwd, setMatchPwd] = useState("");
  const [validMatch, setValidMatch] = useState(false);
  const [matchFocus, setMatchFocus] = useState(false);

  const [gender, setGender] = useState("male");

  const [errMsg, setErrMsg] = useState("");
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    userRef.current.focus();
  }, []);

  useEffect(() => {
    setValidName(USER_REGEX.test(username));
    setValidFn(USER_REGEX.test(first_name));
    setValidLn(USER_REGEX.test(last_name));
    setValidEmail(EMAIL_REGEX.test(email));
    setValidPhone(PHONE_REGEX.test(phone));
    setValidAddress(ADDRESS_REGEX.test(address));
    setValidBirth(BIRTH_REGEX.test(birth));
  }, [username, first_name, last_name, email, phone, address, birth]);

  useEffect(() => {
    setValidPwd(PWD_REGEX.test(password));
    setValidMatch(password === matchPwd);
  }, [password, matchPwd]);

  useEffect(() => {
    setErrMsg("");
  }, [
    username,
    password,
    matchPwd,
    first_name,
    last_name,
    phone,
    email,
    address,
    birth,
  ]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const v1 = USER_REGEX.test(username);
    const v2 = PWD_REGEX.test(password);
    const v3 = USER_REGEX.test(first_name);
    const v4 = USER_REGEX.test(last_name);
    const v5 = EMAIL_REGEX.test(email);
    const v6 = PHONE_REGEX.test(phone);
    const v7 = ADDRESS_REGEX.test(address);
    const v8 = BIRTH_REGEX.test(birth);

    if (!v1 || !v2 || ((!v3 || !v4) && (!v5 || !v6)) || !v7 || !v8) {
      setErrMsg("Invalid Entry");
      return;
    }
    try {
      let newdate = birth.split("/").reverse().join("-");
      let milisbirth = new Date(newdate).getTime();
      const response = await axios.post(
        REGISTER_URL,
        JSON.stringify({
          first_name,
          last_name,
          username,
          email,
          password,
          phone,
          gender,
          birth: milisbirth,
          address,
          typeUser: "user",
        }),
        {
          headers: { "Content-Type": "application/json" },
          withCredentials: false,
        }
      );
      console.log(response?.data);
      console.log(response?.accessToken);
      console.log(JSON.stringify(response));
      setSuccess(true);
      //clear state and controlled input
      setUser("");
      setPwd("");
      setMatchPwd("");
      setFn("");
      setLn("");
      setEmail("");
      setPhone("");
      setAddress("");
      setBirth("");
    } catch (err) {
      if (!err?.response) {
        setErrMsg("No Server Response");
      } else if (err.response?.status === 409) {
        setErrMsg("Username Taken");
      } else {
        setErrMsg("Registration Failed");
      }
      errRef.current.focus();
    }
  };

  return (
    <div className="mainbackground">
      {success ? (
        <section className="sections">
          <h1>Success!</h1>
          <p>
            <a href="/login">Log In</a>
          </p>
        </section>
      ) : (
        <section className="sections">
          <p
            ref={errRef}
            className={errMsg ? "errmsg" : "offscreen"}
            aria-live="assertive"
          >
            {errMsg}
          </p>
          <h1>Register</h1>
          <form className="forms" onSubmit={handleSubmit}>
            <label htmlFor="username">
              Username:
              <FontAwesomeIcon
                icon={faCheck}
                className={validName ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validName || !username ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="username"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setUser(e.target.value)}
              value={username}
              required
              aria-invalid={validName ? "false" : "true"}
              aria-describedby="uidnote"
              onFocus={() => setUserFocus(true)}
              onBlur={() => setUserFocus(false)}
            />
            <p
              id="uidnote"
              className={
                userFocus && username && !validName
                  ? "instructions"
                  : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              4 to 24 characters.
              <br />
              Must begin with a letter.
              <br />
              Letters, numbers, underscores, hyphens allowed.
            </p>

            <label htmlFor="firstname">
              Firstname:
              <FontAwesomeIcon
                icon={faCheck}
                className={validFn ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validFn || !first_name ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="firstname"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setFn(e.target.value)}
              value={first_name}
              required
              aria-invalid={validFn ? "false" : "true"}
              aria-describedby="fnnote"
              onFocus={() => setFnFocus(true)}
              onBlur={() => setFnFocus(false)}
            />
            <p
              id="fnnote"
              className={
                fnFocus && first_name && !validFn ? "instructions" : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              3 to 10 characters.
              <br />
            </p>

            <label htmlFor="lastname">
              Lastname:
              <FontAwesomeIcon
                icon={faCheck}
                className={validLn ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validLn || !last_name ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="lastname"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setLn(e.target.value)}
              value={last_name}
              required
              aria-invalid={last_name ? "false" : "true"}
              aria-describedby="lnnote"
              onFocus={() => setLnFocus(true)}
              onBlur={() => setLnFocus(false)}
            />
            <p
              id="lnnote"
              className={
                lnFocus && last_name && !validLn ? "instructions" : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              3 to 10 characters.
              <br />
            </p>

            <label htmlFor="email">
              Email:
              <FontAwesomeIcon
                icon={faCheck}
                className={validEmail ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validEmail || !email ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="email"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              required
              aria-invalid={validEmail ? "false" : "true"}
              aria-describedby="emailnote"
              onFocus={() => setEmailFocus(true)}
              onBlur={() => setEmailFocus(false)}
            />
            <p
              id="fnnote"
              className={
                emailFocus && email && !validEmail
                  ? "instructions"
                  : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              Email is invalid
              <br />
            </p>

            <label htmlFor="phone">
              Phone Number:
              <FontAwesomeIcon
                icon={faCheck}
                className={validPhone ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validPhone || !phone ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="phone"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setPhone(e.target.value)}
              value={phone}
              required
              aria-invalid={validEmail ? "false" : "true"}
              aria-describedby="phonenote"
              onFocus={() => setPhoneFocus(true)}
              onBlur={() => setPhoneFocus(false)}
            />
            <p
              id="fnnote"
              className={
                phoneFocus && phone && !validPhone
                  ? "instructions"
                  : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              Phone number is invalid
              <br />
            </p>

            <label htmlFor="address">
              Address:
              <FontAwesomeIcon
                icon={faCheck}
                className={validAddress ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validAddress || !address ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="address"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setAddress(e.target.value)}
              value={address}
              required
              aria-invalid={validAddress ? "false" : "true"}
              aria-describedby="addressnote"
              onFocus={() => setAddressFocus(true)}
              onBlur={() => setAddressFocus(false)}
            />
            <p
              id="fnnote"
              className={
                addressFocus && address && !validAddress
                  ? "instructions"
                  : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              Please fill the address
              <br />
            </p>

            <label htmlFor="birth">
              Day Of Birth:
              <FontAwesomeIcon
                icon={faCheck}
                className={validBirth ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validBirth || !birth ? "hide" : "invalid"}
              />
            </label>
            <input
              type="text"
              id="birth"
              ref={userRef}
              autoComplete="off"
              onChange={(e) => setBirth(e.target.value)}
              value={birth}
              required
              aria-invalid={validBirth ? "false" : "true"}
              aria-describedby="birthnote"
              onFocus={() => setBirthFocus(true)}
              onBlur={() => setBirthFocus(false)}
            />
            <p
              id="birthnote"
              className={
                birthFocus && birth && !validBirth
                  ? "instructions"
                  : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              Must DD/MM/YYYY
            </p>

            <div
              className="d-flex"
              style={{ justifyContent: "space-between", alignItem: "center" }}
            >
              <label>Gender :</label>
              <div>
                <input
                  type="radio"
                  name="gender"
                  id="rd1"
                  value="male"
                  onChange={(e) => setGender(e.target.value)}
                />
                <label htmlFor="rd1">Male </label>
              </div>
              <div>
                <input
                  type="radio"
                  name="gender"
                  id="rd2"
                  value="female"
                  onChange={(e) => setGender(e.target.value)}
                />
                <label htmlFor="rd2">Female</label>
              </div>
            </div>

            <label htmlFor="password">
              Password:
              <FontAwesomeIcon
                icon={faCheck}
                className={validPwd ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validPwd || !password ? "hide" : "invalid"}
              />
            </label>
            <input
              type="password"
              id="password"
              onChange={(e) => setPwd(e.target.value)}
              value={password}
              required
              aria-invalid={validPwd ? "false" : "true"}
              aria-describedby="pwdnote"
              onFocus={() => setPwdFocus(true)}
              onBlur={() => setPwdFocus(false)}
            />
            <p
              id="pwdnote"
              className={pwdFocus && !validPwd ? "instructions" : "offscreen"}
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              8 to 24 characters.
              <br />
              Must include uppercase and lowercase letters, a number and a
              special character.
              <br />
              Allowed special characters:{" "}
              <span aria-label="exclamation mark">!</span>{" "}
              <span aria-label="at symbol">@</span>{" "}
              <span aria-label="hashtag">#</span>{" "}
              <span aria-label="dollar sign">$</span>{" "}
              <span aria-label="percent">%</span>
            </p>

            <label htmlFor="confirm_pwd">
              Confirm Password:
              <FontAwesomeIcon
                icon={faCheck}
                className={validMatch && matchPwd ? "valid" : "hide"}
              />
              <FontAwesomeIcon
                icon={faTimes}
                className={validMatch || !matchPwd ? "hide" : "invalid"}
              />
            </label>
            <input
              type="password"
              id="confirm_pwd"
              onChange={(e) => setMatchPwd(e.target.value)}
              value={matchPwd}
              required
              aria-invalid={validMatch ? "false" : "true"}
              aria-describedby="confirmnote"
              onFocus={() => setMatchFocus(true)}
              onBlur={() => setMatchFocus(false)}
            />
            <p
              id="confirmnote"
              className={
                matchFocus && !validMatch ? "instructions" : "offscreen"
              }
            >
              <FontAwesomeIcon icon={faInfoCircle} />
              Must match the first password input field.
            </p>

            <button
              disabled={!validName || !validPwd || !validMatch ? true : false}
            >
              Sign Up
            </button>
          </form>
          <p>
            Already registered?
            <br />
            <span className="line">
              {/*router link*/}
              <a href="/login">Log In</a>
            </span>
          </p>
        </section>
      )}
    </div>
  );
};

export default Register;
