import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import "../../assets/styles/NavigationBar.css";
import axios from "axios";
import jwt from "jwt-decode";
import { useState, useEffect } from "react";
import { useCookies } from "react-cookie";

function NavigationBar() {
  const [cookies, setCookies] = useCookies();
  const [username, setUsername] = useState("");
  const [typeUser, setTypeUser] = useState("user");

  useEffect(() => {
    getUsernameById();
  }, []);

  const getUsernameById = async () => {
    const token = cookies.token;
    const decodeToken = jwt(token);
    const id = decodeToken.id;

    const response = await axios.get(`/api/user/${id}`, {
      withCredentials: false,
    });
    console.log(response.data);
    setUsername(response.data.username);
    setTypeUser(response.data.typeUser);
  };

  function logOut() {
    eraseCookie("token");
    window.location.href = "/";
  }
  function eraseCookie(name) {
    document.cookie =
      name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  }
  return (
    <Navbar collapseOnSelect expand="lg" variant="dark" id="navigation">
      <Container fluid>
        <Navbar.Brand href="/">LOGO</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="nav-left">
            <Nav.Link href="/">HOME</Nav.Link>
            <Nav.Link href="#">WORK</Nav.Link>
            <Nav.Link href="#">CONTACT</Nav.Link>
            <Nav.Link href="#">ABOUT ME</Nav.Link>
          </Nav>
          <Nav className="nav-right">
            {typeUser === "admin" ? (
              <button style={{ backgroundColor: "#FFCC00" }}>
                <Nav.Link href="/admin">Admin</Nav.Link>
              </button>
            ) : (
              <Nav.Link href="#"></Nav.Link>
            )}
            {username ? (
              <Nav.Link href="/user-profile">{username} Profile</Nav.Link>
            ) : (
              <Nav.Link href="/signup">SIGN UP</Nav.Link>
            )}
            {username ? (
              <Nav.Link href="#" onClick={() => logOut()}>
                LOG OUT
              </Nav.Link>
            ) : (
              <Nav.Link href="/login">LOGIN</Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavigationBar;
