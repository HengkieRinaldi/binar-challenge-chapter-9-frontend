import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Card, Button, Container, Form, Alert } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { LoginUser, reset } from "../features/Auth";
import "../assets/styles/Login.css";
import { GoogleButton } from "react-google-button";
import app, { auth } from "../assets/js/firebase";
import { signInWithPopup, GoogleAuthProvider } from "firebase/auth";

const LoginPage = () => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { user, isError, isSuccess, isLoading, message } = useSelector(
    (state) => state.auth
  );
  const provider = new GoogleAuthProvider();
  // provider.addScope("https://www.googleapis.com/auth/contacts.readonly");

  //ini google sign in button, silahkan dimodifikasi untuk cek loginnnya.
  function googleSignIn() {
    signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        console.log(
          "Google Login Log__________________________________________"
        );
        console.log("Ini User :", user);
        console.log("Ini DisplayName :", user.displayName);
        console.log("Ini email :", user.email);
        console.log("Ini Result :", result);
        console.log("Ini Token :", token);
        console.log(
          "Google Login Log__________________________________________"
        );
        // setCookie("user-data", token);
        // ...
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      });
  }
  function setCookie(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }
  useEffect(() => {
    if (user || isSuccess) {
      navigate("/game");
    }
    dispatch(reset());
  }, [user, isSuccess, dispatch, navigate]);

  const Auth = (e) => {
    e.preventDefault();
    dispatch(LoginUser({ username, password }));
  };

  console.log("login", isError);

  return (
    <section className="mainBackgroundLoginPage">
      <Container>
        <div>
          <Link to={"/"}>
            <h3
              style={{
                color: "#FBBC05",
                fontWeight: "bold",
                fontSize: "36px",
                marginTop: "1rem",
                textAlign: "inherit",
              }}
            >
              Traditional Game
            </h3>
          </Link>
        </div>
        <div className="mainAllCard">
          <Card
            style={{
              marginTop: "24px",
              width: "25rem",
              height: "30rem",
            }}
          >
            <Card.Body>
              <Form onSubmit={Auth}>
                {isError && <p className="has-text-centered">{message}</p>}
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>User name</Form.Label>
                  <Form.Control
                    style={{
                      fontSize: "15px",
                      color: "#D0D0D0",
                      paddingLeft: "0.75rem 1.5rem",
                    }}
                    type="text"
                    className="input"
                    value={username}
                    onChange={(e) => setUserName(e.target.value)}
                    placeholder="Enter username"
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <div className="d-flex justify-content-between align-items-center">
                    <Form.Label>Password</Form.Label>
                    <div className="forgotpassword">
                      <Alert.Link href="/">forgot password?</Alert.Link>
                    </div>
                  </div>
                  <Form.Control
                    style={{
                      fontSize: "15px",
                      padding: "6px 12px",
                    }}
                    type="password"
                    className="input"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Password"
                  />
                </Form.Group>

                <div className="d-grid gap-2">
                  <Button
                    style={{
                      padding: "0.5rem 2rem",
                      margin: "1rem 0rem",
                      width: "",
                    }}
                    variant="primary"
                    type="submit"
                    size="md"
                  >
                    {isLoading ? "Loading..." : "Sign In"}
                  </Button>
                </div>

                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                  <div className="d-flex align-items-center">
                    <Form.Check type="checkbox" />
                    <h3
                      style={{
                        marginLeft: ".75rem",
                        fontSize: "12px",
                      }}
                    >
                      Stay signed in
                    </h3>
                  </div>
                </Form.Group>
              </Form>
              <br></br>

              <div className="d-flex justify-content-between align-items-center">
                <hr
                  style={{
                    display: "block",
                    backgroundColor: "#D0D0D0",
                    border: 0,
                    width: "7rem",
                  }}
                />
                <p
                  style={{
                    fontSize: "14px",
                    color: "#D0D0D0",
                  }}
                >
                  or Sign in with
                </p>

                <hr
                  style={{
                    display: "block",
                    backgroundColor: "#D0D0D0",
                    border: 0,
                    width: "7em",
                  }}
                />
              </div>
              <GoogleButton onClick={googleSignIn} />
            </Card.Body>
          </Card>

          <Card
            style={{
              marginTop: "24px",
              width: "25rem",
            }}
          >
            <Card.Body>
              <div className="flex text-center">
                <Card.Text className="mb-2 text-muted">
                  New to traditional game?
                </Card.Text>
                <div className="Createanaccount">
                  <Card.Link href="/signup">Create an account</Card.Link>
                </div>
              </div>
            </Card.Body>
          </Card>
        </div>
      </Container>
    </section>
  );
};

export default LoginPage;
