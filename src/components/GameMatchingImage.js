import '../assets/styles/GameMatchingImage.css';
import MatchingImages from './MatchingImages';
import {useState} from "react";
import {shuffle} from 'lodash';

function GameMatchingImages() {
  const [cards,setCards] = useState( shuffle([...MatchingImages, ...MatchingImages]) );
  const [clicks,setClicks] = useState(0);
  const [won,setWon] = useState(false);
  const [activeCards,setActiveCards] = useState([]);
  const [foundPairs,setFoundPairs] = useState([]);

  function flipCard(index) {
    if (won) {
      setCards(shuffle([...MatchingImages, ...MatchingImages]));
      setFoundPairs([]);
      setWon(false);
      setClicks(0);
    }
    if (activeCards.length === 0) {
      setActiveCards([index]);
    }
    if (activeCards.length === 1) {
      const firstIndex = activeCards[0];
      const secondsIndex = index;
      if (cards[firstIndex] === cards[secondsIndex]) {
        if (foundPairs.length + 2 === cards.length) {
          setWon(true);
        }
        setFoundPairs( [...foundPairs, firstIndex, secondsIndex] );
      }
      setActiveCards([...activeCards, index]);
    }
    if (activeCards.length === 2) {
      setActiveCards([index]);
    }
    setClicks(clicks + 1);
  }

  return (
    <div id='body-GameMatchingImage'>
      <h1 className="justify-content-center text-center text-light pb-3">matching games</h1>
      <div className="board">
        {cards.map((card,index) => {
          const flippedToFront =  (activeCards.indexOf(index) !== -1) || foundPairs.indexOf(index) !== -1;
          return (
            <div className={"card-outer " + (flippedToFront ? 'flipped' : '')}
                 onClick={() => flipCard(index)}>
              <div className="card-matching">
                <div className="front">
                  <img src={card} alt='gambar game' />
                </div>
                <div className="back" />
              </div>
            </div>
          );
        })}
      </div>
      <div className="stats">
        {won && (
          <>You won the game! Congratulations!<br />
            Click any card to play again.<br /><br />
          </>
        )}
        Clicks: {clicks}  Found pairs:{foundPairs.length/2}
      </div>
    </div>
  );
}

export default GameMatchingImages;
