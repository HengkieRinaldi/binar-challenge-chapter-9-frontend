import { BrowserRouter, Routes, Route } from "react-router-dom";
import Admin from "./components/Admin";
import EditUser from "./components/EditUser";
import Game from "./components/Game";
import GameMatchingImage from "./components/GameMatchingImage";
import GameTicTacToe from "./components/GameTicTacToe";
import Homepage from "./components/Homepage";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import UserProfile from "./components/UserProfile";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/login" element={<Login />} />
        <Route path="/game" element={<Game />} />
        <Route path="/user-profile" element={<UserProfile />} />
        <Route path="/admin" element={<Admin />} />
        <Route path="/user-edit/:id" element={<EditUser />} />
        <Route path="/matching-images" element={< GameMatchingImage />} />
        <Route path="/tictactoe" element={< GameTicTacToe />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
