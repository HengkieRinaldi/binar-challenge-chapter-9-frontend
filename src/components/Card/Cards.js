import React from "react";
import '../../assets/styles/Cards.css'

function Cards() {
    return (
        <>
            <div class="row justify-content-end" id="card1">
                <div class="col-sm-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex justify-content-between">         
                                <div class="image-profile d-flex">
                                    <img src={require('../../assets/img/evan-lahti.jpg')} alt="" />         
                                    <div class="pl-3">
                                        <p class="text-orange">Evan Lathi</p>
                                        <p class="sub-text">PC Gamer</p>
                                    </div>
                                </div>  
                                <img src={require('../../assets/img/twitter.svg').default} alt="" />
                            </div>
                            <p class="card-text">"One of my gaming highlights of the year."</p>
                            <p class="sub-text" id="date">June 18, 2021</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row my-3" id="card2">
                <div class="col-sm-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex justify-content-between">         
                                <div class="image-profile d-flex">
                                    <img src={require('../../assets/img/jada-griffin.jpg')} alt="" />         
                                    <div class="pl-3">
                                        <p class="text-orange">Jada Griffin</p>
                                        <p class="sub-text">Nerdreactor</p>
                                    </div>
                                </div>  
                                <img src={require('../../assets/img/twitter.svg').default} alt="" />
                            </div>
                            <p class="card-text">The next big thing in the world of streaming and survival games."</p>
                            <p class="sub-text" id="date">July 10, 2021</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-end" id="card3">
                <div class="col-sm-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex justify-content-between">         
                                <div class="image-profile d-flex">
                                    <img src={require('../../assets/img/aaron-williams.jpg')} alt="" />         
                                    <div class="pl-3">
                                        <p class="text-orange">Evan Lathi</p>
                                        <p class="sub-text">Uprox</p>
                                    </div>
                                </div>  
                                <img src={require('../../assets/img/twitter.svg').default} alt="" />
                            </div>
                            <p class="card-text">"Snoop Dogg Playing The Wildly Entertaining 'SOS' is Ridiculous."</p>
                            <p class="sub-text" id="date">December 24, 2018</p>
                        </div>
                    </div>
                </div>
            </div>
        
        </>  
    )
}

export default Cards