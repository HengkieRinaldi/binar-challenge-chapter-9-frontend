import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import NavigationBar from "../components/Navbar/NavigationBar";
import "bootstrap/dist/css/bootstrap.min.css";
import { FaHandRock, FaHandPaper, FaHandScissors } from "react-icons/fa";
import { useState } from "react";
import "../assets/styles/Game.css";
import { useCookies } from "react-cookie";
import jwt from "jwt-decode";
import axios from "axios";

const actions = {
  rock: ["scissors"],
  paper: ["rock"],
  scissors: ["paper"],
};

function randomAction() {
  const keys = Object.keys(actions);
  const index = Math.floor(Math.random() * keys.length);

  return keys[index];
}

function calculateWinner(action1, action2) {
  if (action1 === action2) {
    return 0;
  } else if (actions[action1].includes(action2)) {
    return -1;
  } else if (actions[action2].includes(action1)) {
    return 1;
  }

  return null;
}

function ActionIcon({ action, ...props }) {
  const icons = {
    rock: FaHandRock,
    paper: FaHandPaper,
    scissors: FaHandScissors,
  };
  const Icon = icons[action];
  return <Icon {...props} size="100px" />;
}

function Player({ name = "Player", score = 0, action = "rock" }) {
  return (
    <div className="player">
      <div className="score">
        <p>Score {`${name}: ${score}`}</p>
      </div>
      <div className="action">
        {action && <ActionIcon action={action} size={60} />}
      </div>
    </div>
  );
}

function ActionButton({ action = "rock", onActionSelected }) {
  return (
    <button className="round-btn" onClick={() => onActionSelected(action)}>
      <ActionIcon action={action} size={20} />
    </button>
  );
}

function ShowWinner({ winner = 0 }) {
  const text = {
    "-1": "You Win!",
    0: "It's a Tie",
    1: "Computer Win!",
  };

  return <h2 className="textWin">{text[winner]}</h2>;
}

function Game() {
  const [cookies, setCookies] = useCookies();
  const [playerAction, setPlayerAction] = useState("");
  const [computerAction, setComputerAction] = useState("");

  const [playerScore, setPlayerScore] = useState(0);
  const [computerScore, setComputerScore] = useState(0);
  const [winner, setWinner] = useState(0);

  const onActionSelected = (selectedAction) => {
    const newComputerAction = randomAction();

    setPlayerAction(selectedAction);
    setComputerAction(newComputerAction);

    const newWinner = calculateWinner(selectedAction, newComputerAction);
    setWinner(newWinner);
    if (newWinner === -1) {
      setPlayerScore(playerScore + 1);
    } else if (newWinner === 1) {
      setComputerScore(computerScore + 1);
    }
    console.log(playerScore);
    console.log(computerScore);
  };

  const resetScore = () => {
    setPlayerScore(0);
    setComputerScore(0);
    playerAction("");
    computerAction("");
    ShowWinner("");
  };

  const sendScore = async () => {
    const token = cookies.token;
    const user = jwt(token);
    const req_id = user.id;
    const req_username = user.username;

    console.log(req_id, "  ", req_username);

    try {
      const response = await axios.post(
        `/api/history`,
        {
          user_id: req_id,
          username: req_username,
          win: playerScore,
          draw: 0,
          lose: computerScore,
          scheme: "user vs bot",
          oponent: "computer or bot",
          timestamp: Date.now(),
        },
        {
          headers: { "Content-Type": "application/json" },
          withCredentials: false,
        }
      );
      console.log({
        message: "success to send data",
        statusCode: 200,
        result: response,
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="App">
      <NavigationBar />
      <Container fluid className="suit">
        <Row id="game">
          <Col id="choose">
            <h3>ROCK PAPER SCISSORS</h3>
            <div className="chooseObj">
              <ActionButton action="rock" onActionSelected={onActionSelected} />
              <ActionButton
                action="paper"
                onActionSelected={onActionSelected}
              />
              <ActionButton
                action="scissors"
                onActionSelected={onActionSelected}
              />
            </div>
          </Col>

          <Col id="result">
            <div className="resultGame">
              <ShowWinner winner={winner} />
            </div>
            <button className="resetButton" onClick={resetScore}>
              Reset Score{" "}
            </button>
            <button className="sendButton" onClick={sendScore}>
              Send Score
            </button>
          </Col>

          <Col id="scoreCard">
            <div className="scoreResult">
              <Player name="Player" score={playerScore} action={playerAction} />
              <p className="vs">VS</p>
              <Player
                name="Computer"
                score={computerScore}
                action={computerAction}
              />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Game;
